import './css/App.css';
import Viewer from './../lib';

class App {
  constructor(){
    let instance = new Viewer(
      "viewer",
      "http://localhost:5000/api/search",
      {
        map: {
          startWide: true
        }
      }
    );

    instance.on("picture-loaded", (e, data) => {
      console.log("Switching to picture", data.picId, "at position lat", data.lat, "lon", data.lon, "heading to", data.x, "degrees");
    });

    instance.on("picture-tiles-loaded", (e, data) => {
      console.log("All tiles loaded for picture", data.picId);
    });

    instance.on("view-rotated", (e, position) => {
      console.log("New heading", position.x);
    });

    console.log("Map center", instance.getMap().getCenter());
  }
}

export default App;
