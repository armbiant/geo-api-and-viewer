# GeoVisio

(Documentation for NPM publishing)

GeoVisio is a complete solution for storing and __serving your own geolocated pictures__ (like [StreetView](https://www.google.com/streetview/) / [Mapillary](https://mapillary.com/)).

Give it a try at [geovisio.fr](https://geovisio.fr/viewer) !

## GeoVisio viewer library

The GeoVisio viewer is a web JS library which displays pictures and sequences from a GeoVisio server, or any STAC API offering geolocated pictures.

All information and documentation is available at https://gitlab.com/geovisio/geovisio

## License

Copyright (c) Adrien Pavie 2022-2023, [released under MIT license](https://gitlab.com/geovisio/geovisio/-/blob/develop/LICENSE).
