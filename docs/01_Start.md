# GeoVisio hands-on guide

![GeoVisio logo](../images/logo_full.png)

Welcome to GeoVisio documentation ! It will help you through all phases of install, setup, run and development of GeoVisio.

GeoVisio is split into two components :
- __Server__ : the API and backend
- __Client__ : the web map & pictures viewer

They can be used independently or together according to your needs. You might want to dive into docs :

- __Server-side__
  - Deploy an instance
    - [Using Docker](./10_Install_Docker.md)
    - Using a classic install
      - [Setup your database](./07_Database_setup.md)
      - [Install Geovisio and dependencies](./10_Install_Classic.md)
    - [Using Scalingo solutions](./10_Install_Scalingo.md)
  - [Available settings](./11_Server_settings.md)
  - [Organize your pictures and sequences](./12_Pictures_storage.md)
  - Start and use your server
    - [Classic-deployed](./15_Running_Classic.md)
    - [Docker-deployed](./15_Running_Docker.md)
  - [Work with HTTP API](./16_Using_API.md)
  - Advanced server topics
    - [Available blur algorithms](./17_Blur_algorithms.md)
    - [Developing on the server](./19_Develop_server.md)
- __Client-side__
  - [Quickstart](./20_Client_quickstart.md)
  - [Viewer JS methods](./21_Client_usage.md)
  - [Viewer URL settings](./22_Client_URL_settings.md)
  - [STAC API compatibility notes](./25_Viewer_compatibility.md)
  - [Developing on the viewer](./29_Develop_client.md)

If at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/geovisio/-/issues) or by [email](mailto:panieravide@riseup.net).
