# Organize your pictures and sequences

GeoVisio server main task is to offer your own pictures through API. To do so, it needs to access your pictures, and they should follow a certain structure. This documentation lists requirements on pictures and sequences (serie of pictures).

### Prepare your pictures

GeoVisio has several prerequisites for a pictures to be accepted, mainly concerning availability of some [EXIF tags](https://en.wikipedia.org/wiki/Exif). A picture __must__ have the following EXIF tags defined:

- GPS coordinates with `GPSLatitude, GPSLatitudeRef, GPSLongitude, GPSLongitudeRef`
- Date, either with `GPSTimeStamp` or `DateTimeOriginal`

The following EXIF tags are recognized and used if defined, but are __optional__:

- Image orientation, either with `GPSImgDirection` or `GPano:PoseHeadingDegrees` (was mandatory in versions <= 1.3.1)
- Milliseconds in date with `SubSecTimeOriginal`
- If picture is 360° / spherical with `GPano:ProjectionType`
- Camera model with `Make, Model`
- Camera focal length (to get precise field of view) with `FocalLength`

Note that GeoVisio now accepts both __360° and classic/flat pictures__ (versions <= 1.2.0 only supported 360° pictures).

### Organize your pictures

For GeoVisio to recognize your pictures and sequences, your instance root folder (`FS_URL` parameter) should contain a list of subfolders (one per sequence), and each subfolder should contain a list of pictures (JPEG files, read in alphabetical order). As an example of tree hierarchy:

- Main folder (according to your `FS_URL` parameter)
  - `my_seq_1` : one sequence folder
    - `my_pic_1.jpg`
    - `my_pic_2.jpg`
    - `my_pic_3.jpg`
    - ...
  - `my_seq_2` : another sequence
    - And its pictures...
  - ...

Once this directory is ready, the `process-sequences` will look through it and parse necessary metadata. Note that original pictures and folder structure __will not be changed__. Although, GeoVisio will add various automatically generated files, all prefixed with `geovisio_` so you can easily clean-up if necessary. Also note that all sequences folder with a name starting with `geovisio_`, `gvs_` or `ignore_` will not be processed, allowing to put aside some sequences you don't want to be processed at some point.

#### metadata.txt configuration file

Each sequence folder can contain a `metadata.txt` file.

This key-value file is used to associate metadata to a sequence. The keys are separated from the values by `=` and values may contain spaces (no quoting is necessary).

```metadata.txt
account-name = some account name
```

Possible metadata:
* `account-name`: associate each pictures to the account provided. If not defined, the pictures are associated to the instance's default account.
* `account-id`: there are no constraint on the name being unique since in some cases it can be provided by an external identity provider. If there is an ambiguity with the name, use `account-id` instead of `account-name`.


## Next step

You are ready now for starting your server: [Docker-deployed](./15_Running_Docker.md) or [Classic-deployed](./15_Running_Classic.md).
