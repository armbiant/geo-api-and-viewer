from urllib.parse import urlparse, quote, unquote
import requests
import pytest
from src import auth

# mark all tests in the module with the docker marker
pytestmark = [pytest.mark.docker, pytest.mark.skipci]


def test_auth_deactivated(client):
	# by default the auth is not activated, so the login routes should not exists
	assert client.get("/auth/login").status_code == 404
	assert client.get("/auth/redirect").status_code == 404
	assert client.get("/auth/logout").status_code == 404


def test_login(auth_client, auth_app):
	response = auth_client.get("/auth/login")
	assert response.status_code == 302

	location = response.headers["Location"]
	parsed_url = urlparse(location)

	assert parsed_url.path == "/realms/geovisio/protocol/openid-connect/auth"

	queries = {s.split("=")[0]: s.split("=")[1] for s in parsed_url.query.split("&")}

	assert queries["response_type"] == "code"
	assert queries["code_challenge_method"] == "S256"
	assert queries["client_id"] == "geovisio"
	assert "code_challenge" in queries
	assert "state" in queries
	assert "nonce" in queries
	assert queries["scope"] == "openid"
	assert queries["redirect_uri"] == quote(
		f"http://{auth_app.config['SERVER_NAME']}/auth/redirect", safe=""
	)


def _get_keycloak_authenticate_form_url(response):
	"""Little hack to parse keycloak HTML to get the url to the authenticate form"""
	import re

	url = re.search('action="(.*login-actions/authenticate[^"]*)"', response.text)
	assert url
	url = url.group(1).replace("&amp;", "&")
	return url


def test_login_with_redirect(server, keycloak, auth_app):
		
	# we mount a testing route that required login
	@auth_app.route("/protected_api")
	@auth.login_required
	def protected_api_test(account):
		return f"hi {account.name}"

	with requests.session() as s:
		# we do a first query to login (following redirect ) (inside a requests session to keep the cookies)
		login = s.get(f"{server}/auth/login", allow_redirects=True)
		login.raise_for_status()
		assert login.status_code == 200
		assert len(login.history) == 1

		# Then we authenticate on the keycloak to an already created user (defined in 'keycloak-realm.json')
		url = _get_keycloak_authenticate_form_url(login)
		r = s.post(
			url,
			data={"username": "elysee", "password": "my password"},
			headers={"Content-Type": "application/x-www-form-urlencoded"},
			allow_redirects=True,
		)
		r.raise_for_status()
		account = r.json()["account"]
		assert "id" in account
		assert account["name"] == "Elysee r"
		assert account["oauth_provider"] == "keycloak"
		assert "oauth_id" in account

		# Once logged in, we can query the protected api (using the session cookie)
		r = s.get(f"{server}/protected_api")
		assert len(r.history) == 0
		r.raise_for_status()
		assert r.text == "hi Elysee r"

		# we log out of the server
		r = s.get(f"{server}/auth/logout")
		r.raise_for_status()

		# then the next calls to /protected_api will relauch the oauth dance
		# Note: since for the moment we do not logout from the oauth provider
		# since it's not yet implemented in authlib (https://github.com/lepture/authlib/issues/292)
		# The oauth dance will succeed without having to post the keycloak form
		# If we handle a real logout, this test will have to be modified
		r = s.get(f"{server}/protected_api", allow_redirects=True)

		parsed_history = [urlparse(h.url).path for h in r.history]
		assert parsed_history == [
			"/protected_api",
			"/auth/login",
			"/realms/geovisio/protocol/openid-connect/auth",
			"/auth/redirect",
		]
		assert r.text == "hi Elysee r"
