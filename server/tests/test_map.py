from flask import json
import os
import pytest
import psycopg
import mapbox_vector_tile
from . import conftest
from src import create_app, map


FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)


@pytest.mark.parametrize(('z', 'x', 'y', 'format', 'result'), (
	( 6,  0,  0, 'mvt', True),
	( 6,  0,  1, 'mvt', True),
	( 6,  1,  0, 'mvt', True),
	( 6,  1,  1, 'mvt', True),
	(-1,  0,  0, 'mvt',  404),
	(16,  0,  0, 'mvt',  404),
	( 6, -1,  0, 'mvt',  404),
	( 6,  64,  0, 'mvt',  404),
	( 6,  0, -1, 'mvt',  404),
	( 6,  0,  64, 'mvt',  404),
	( 6,  0,  0, 'jpg',  400),
	(None, 0, 0, 'jpg',  400),
	( 6, None, 0, 'jpg', 400),
	( 6, 0, None, 'jpg', 400),
	( 6,  0,  0,  None,  400),
))
def test_isTileValid(z, x, y, format, result):
	if result is True:
		assert map.isTileValid(z, x, y, format) is True
	else:
		with pytest.raises(Exception) as e_info:
			map.isTileValid(z, x, y, format)
			assert e_info.status_code == result


@conftest.SEQ_IMGS
@pytest.mark.parametrize(('z', 'x', 'y', 'layersCount'), (
	(14, 8279, 5626, {'pictures': 5, 'sequences': 1}),
	(11, 1034,  703, {'sequences': 1}),
	(11,    0,    0, {}),
 	( 6,   32,   21, {}), # No sequences due to simplification
 	( 0,    0,    0, {}), # No sequences due to simplification
))
def test_getTile(datafiles, initSequence, z, x, y, layersCount):
	client = initSequence(datafiles, preprocess = False)

	response = client.get(f"/api/map/{z}/{x}/{y}.mvt")

	assert response.status_code == 200
	data = mapbox_vector_tile.decode(response.get_data())

	for layerName, layerCount in layersCount.items():
		assert layerName in data
		assert len(data[layerName]['features']) == layerCount
