import io
import os
import psycopg
import pytest
from src import create_app
from src.scripts import mock_data
from PIL import Image, ExifTags

FIXTURE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")


def test_generate_mock_data(tmp_path, dburl):
	base_image = os.path.join(FIXTURE_DIR, "d1.jpg")
	destPath = tmp_path / "out"
	destPath.mkdir()
	app = create_app(
		{
			"TESTING": True,
			"DB_URL": dburl,
			"BLUR_STRATEGY": "DISABLE",
			"DERIVATES_STRATEGY": "PREPROCESS",
			"FS_URL": "osfs://" + str(destPath),
		}
	)
	with app.app_context():
		mock_data.fill_with_mock_data(
			base_image_path=base_image,
			nb_sequences=2,
			nb_pictures_per_sequence=3,
			max_size="100MB",
		)

		# 2 sequences should have been created
		sequence_dir = [os.path.join(destPath, s) for s in ["sequence_0", "sequence_1"]]
		assert set(os.listdir(destPath)) == set(
			["sequence_0", "sequence_1", "geovisio_derivates"]
		)
		for dir in sequence_dir:
			images_list = os.listdir(dir)
			assert len(set(images_list)) == 3

			# we check that the exif metadata are different
			images = [Image.open(os.path.join(dir, p)) for p in images_list]
			exifs = [i.getexif() for i in images]

			lats = [
				e.get_ifd(ExifTags.Base.GPSInfo)[ExifTags.GPS.GPSLatitude]
				for e in exifs
			]
			assert lats[0] != lats[1] != lats[2]

			lon = [
				e.get_ifd(ExifTags.Base.GPSInfo)[ExifTags.GPS.GPSLongitude]
				for e in exifs
			]
			assert lon[0] != lon[1] != lon[2]

			time_digitalized = [
				e.get_ifd(ExifTags.Base.ExifOffset)[ExifTags.Base.DateTimeDigitized]
				for e in exifs
			]
			assert time_digitalized[0] != time_digitalized[1] != time_digitalized[2]
			time_original = [
				e.get_ifd(ExifTags.Base.ExifOffset)[ExifTags.Base.DateTimeOriginal]
				for e in exifs
			]
			assert time_original[0] != time_original[1] != time_original[2]

		# all sequences should have been added to the database
		with psycopg.connect(dburl) as db:
			nb_sequences = db.execute("select count(*) from sequences").fetchone()
			assert nb_sequences[0] == 2


def test_generate_mock_data_limit(tmp_path, dburl):
	destPath = tmp_path / "out_limit"
	destPath.mkdir()
	base_image = os.path.join(FIXTURE_DIR, "d1.jpg")

	# compute the real image size since  PIL does some compresion
	bytes = io.BytesIO()
	img = Image.open(base_image)
	img.save(bytes, format='jpeg')
	base_image_size = bytes.getbuffer().nbytes

	nb_wanted_images = 4
	max_size = f"{base_image_size * nb_wanted_images}B"

	app = create_app(
		{
			"TESTING": True,
			"DB_URL": dburl,
			"BLUR_STRATEGY": "DISABLE",
			"DERIVATES_STRATEGY": "PREPROCESS",
			"FS_URL": "osfs://" + str(destPath),
		}
	)
	with app.app_context():
		mock_data.fill_with_mock_data(
			base_image_path=base_image,
			nb_sequences=2,
			nb_pictures_per_sequence=3,
			max_size=max_size,
		)

		# only 4 images should have been created, 3 for the first sequence, and 1 for the second
		assert sorted(os.listdir(destPath)) == sorted(["sequence_0", "sequence_1", "geovisio_derivates"])
		assert len(os.listdir(os.path.join(destPath, "sequence_0"))) == 3
		assert len(os.listdir(os.path.join(destPath, "sequence_1"))) == 1
