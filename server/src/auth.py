import flask
from flask import current_app, url_for, session, redirect, request
import psycopg
from functools import wraps
from authlib.integrations.flask_client import OAuth
from dataclasses import dataclass
import json
from abc import ABC, abstractmethod
from typing import Any

bp = flask.Blueprint("auth", __name__, url_prefix="/auth")

ACCOUNT_KEY = "account"  # Key in flask's session with the account's information
NEXT_URL_KEY = (
	"next-url"  # Key in flask's session with url to be redirected after the oauth dance
)

oauth = OAuth()
oauth_provider = None


@dataclass
class OAuthUserAccount(object):
	id: str
	name: str


class OAuthProvider(ABC):
	"""Base class for oauth provider. Need so specify how to get user's info"""

	name: str
	client: Any

	def __init__(self, name, **kwargs) -> None:
		super(OAuthProvider, self).__init__()
		self.name = name
		self.client = oauth.register(name=name, **kwargs)

	@abstractmethod
	def get_user_oauth_info(self, tokenResponse) -> OAuthUserAccount:
		pass


class OIDCProvider(OAuthProvider):
	def __init__(self, *args, **kwargs) -> None:
		super(OIDCProvider, self).__init__(*args, **kwargs)

	def get_user_oauth_info(self, tokenResponse) -> OAuthUserAccount:
		# user info is alway provided by oidc provider, nothing to do
		# we only need the 'sub' (subject) claim
		oidc_userinfo = tokenResponse["userinfo"]
		return OAuthUserAccount(id=oidc_userinfo["sub"], name=oidc_userinfo["name"])


class KeycloakProvider(OIDCProvider):
	def __init__(self, keycloack_realm_user, client_id, client_secret) -> None:
		super().__init__(
			name="keycloak",
			client_id=client_id,
			client_secret=client_secret,
			server_metadata_url=f"{keycloack_realm_user}/.well-known/openid-configuration",
			client_kwargs={
				"scope": "openid",
				"code_challenge_method": "S256",  # enable PKCE
			},
		)


class OSMOAuthProvider(OAuthProvider):
	def __init__(self, oauth_key, oauth_secret) -> None:
		super().__init__(
			name="osm",
			client_id=oauth_key,
			client_secret=oauth_secret,
			api_base_url="https://api.openstreetmap.org/api/0.6/",
			authorize_url="https://www.openstreetmap.org/oauth2/authorize",
			access_tokenurl="https://www.openstreetmap.org/oauth2/token",
			client_kwargs={
				"scope": "read_prefs",
			},
		)

	def get_user_oauth_info(self, tokenResponse) -> OAuthUserAccount:
		"""Get the id/name of the logged user from osm's API
		cf. https://wiki.openstreetmap.org/wiki/API_v0.6
		Args:
				tokenResponse: access token to the OSM api, will be automatically used to query the OSM API

		Returns:
				OAuthUserAccount: id and name of the account
		"""
		details = self.client.get("user/details.json")
		details.raise_for_status()
		details = details.json()
		return OAuthUserAccount(
			id=str(details["user"]["id"]), name=details["user"]["display_name"]
		)


def make_auth(app):
	def ensure(*app_config_key):
		missing = [k for k in app_config_key if k not in app.config]
		if missing:
			raise Exception(
				f"To setup a keycloak as identity provider, you need to provide {missing} in configuration"
			)

	global oauth_provider
	if app.config.get("OAUTH_PROVIDER") == "oidc":
		ensure("OIDC_URL", "CLIENT_ID", "CLIENT_SECRET")

		oauth_provider = KeycloakProvider(
			app.config["OIDC_URL"],
			app.config["CLIENT_ID"],
			app.config["CLIENT_SECRET"],
		)
	elif app.config.get("OAUTH_PROVIDER") == "osm":
		ensure("CLIENT_ID", "CLIENT_SECRET")

		oauth_provider = OSMOAuthProvider(
			app.config["CLIENT_ID"],
			app.config["CLIENT_SECRET"],
		)
	else:
		raise Exception(
			"Unsupported OAUTH_PROVIDER, should be either 'oidc' or 'osm'. If you want another provider to be supported, add a subclass to OAuthProvider"
		)
	oauth.init_app(app)

	return oauth


@bp.route("/login")
def login():
	next_url = request.args.get("next_url")
	if next_url:
		# we store the next_url in the session, to be able to redirect the users to this url after the oauth dance
		session[NEXT_URL_KEY] = next_url
	return oauth_provider.client.authorize_redirect(
		url_for("auth.auth", _external=True, _scheme=request.scheme)
	)


@dataclass
class Account(object):
	id: str
	name: str
	oauth_provider: str
	oauth_id: str


@bp.route("/redirect")
def auth():
	tokenResponse = oauth_provider.client.authorize_access_token()

	oauth_info = oauth_provider.get_user_oauth_info(tokenResponse)
	with psycopg.connect(current_app.config["DB_URL"]) as conn:
		with conn.cursor() as cursor:
			res = cursor.execute(
				"INSERT INTO accounts (name, oauth_provider, oauth_id) VALUES (%(name)s, %(provider)s, %(id)s) ON CONFLICT (oauth_provider, oauth_id) DO UPDATE SET name = %(name)s RETURNING id, name",
				{
					"provider": oauth_provider.name,
					"id": oauth_info.id,
					"name": oauth_info.name,
				},
			).fetchone()
			if res is None:
				raise Exception("Impossible to insert user in database")
			id, name = res
			account = Account(
				id=str(id),  # convert uuid to string for serialization
				name=name,
				oauth_provider=oauth_provider.name,
				oauth_id=oauth_info.id,
			)
			session[ACCOUNT_KEY] = account.__dict__

			next_url = session.pop(NEXT_URL_KEY, None)
			if next_url:
				return redirect(next_url)
			return flask.jsonify(account=account)


def login_required(f):
	@wraps(f)
	def decorator(*args, **kwargs):
		account = get_current_account()
		if not account:
			return redirect(url_for("auth.login", next_url=request.url))
		kwargs["account"] = account

		return f(*args, **kwargs)

	return decorator


class UnknowAccountException(Exception):
	status_code = 401

	def __init__(self):
		msg = f"No account with this oauth id is know, you should login first"
		super().__init__(msg)


class LoginRequiredException(Exception):
	status_code = 401

	def __init__(self):
		msg = f"You should login to request this API"
		super().__init__(msg)


def get_current_account():
	"""Get the authenticated account information from the flask session

	Returns:
			Account: the current logged account, None if nobody is logged
	"""
	if ACCOUNT_KEY in session:
		session_account = Account(**session[ACCOUNT_KEY])

		return session_account

	return None


@bp.route("/logout")
def logout():
	# TODO should we also logout from Identity Provider?
	session.pop(ACCOUNT_KEY, None)

	return redirect("/")
