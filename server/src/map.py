# Some parts of code here are heavily inspired from Paul Ramsey's work
# See for reference : https://github.com/pramsey/minimal-mvt

import psycopg
import io
from flask import Blueprint, current_app, send_file
from . import errors

bp = Blueprint('map', __name__, url_prefix='/api/map')


def isTileValid(z, x, y, format):
	"""Check if tile parameters are valid

	Parameters
	----------
	z : number
		Zoom level
	x : number
		X coordinate
	y : number
		Y coordinate
	format : string
		Tile format

	Returns
	-------
	boolean
		True if parameters are OK, raises InvalidAPIUsage exceptions otherwise
	"""
	if z is None or x is None or y is None or format is None:
		raise errors.InvalidAPIUsage("One of required parameter is empty", status_code=404)
	if format not in ['pbf', 'mvt']:
		raise errors.InvalidAPIUsage("Tile format is invalid, should be either pbf or mvt", status_code=400)

	size = 2 ** z
	if x >= size or y >= size:
		raise errors.InvalidAPIUsage("X or Y parameter is out of bounds", status_code=404)
	if x < 0 or y < 0:
		raise errors.InvalidAPIUsage("X or Y parameter is out of bounds", status_code=404)
	if z < 0 or z > 14:
		raise errors.InvalidAPIUsage("Z parameter is out of bounds (should be 0-14)", status_code=404)

	return True


@bp.route('/<int:z>/<int:x>/<int:y>.<format>')
def getTile(z: int, x: int, y: int, format: str):
	"""Get pictures and sequences as vector tiles

	Vector tiles contains possibly two layers : sequences and pictures.

	Layer "sequences":
	  - Available on all zoom levels
	  - Available properties: id (sequence ID)

	Layer "pictures":
	  - Available on zoom levels >= 13
	  - Available properties: id (picture ID), ts (picture date/time), heading (picture heading in degrees)
	---
		parameters:
			- name: z
			  in: path
			  type: number
			  description: Zoom level (6 to 14)
			- name: x
			  in: path
			  type: number
			  description: X coordinate
			- name: y
			  in: path
			  type: number
			  description: Y coordinate
			- name: format
			  in: path
			  type: string
			  description: Tile format (mvt, pbf)
		responses:
			200:
				description: Sequences vector tile
				content:
					application/vnd.mapbox-vector-tile:
						schema:
							type: string
							format: binary
	"""

	if isTileValid(z, x, y, format):
		with psycopg.connect(current_app.config['DB_URL'], options="-c statement_timeout=10000") as conn:
			with conn.cursor() as cursor:
				res = cursor.execute(_get_query(z), {"z": z, "x": x, "y": y}).fetchone()[0]

				return send_file(io.BytesIO(res), mimetype='application/vnd.mapbox-vector-tile')

			raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


def _get_query(z: int) -> str:
	"""Returns appropriate SQL query according to given zoom"""

	if z >= 13:
		query = """
SELECT mvtsequences.mvt || mvtpictures.mvt
FROM (
	SELECT ST_AsMVT(mvtgeomseqs.*, 'sequences') AS mvt
	FROM (
		SELECT
			ST_AsMVTGeom(ST_Transform(geom, 3857), ST_TileEnvelope(%(z)s, %(x)s, %(y)s)) AS geom,
			id
		FROM sequences
		WHERE
			status = 'ready'
			AND geom && ST_Transform(ST_TileEnvelope(%(z)s, %(x)s, %(y)s), 4326)
	) mvtgeomseqs
) mvtsequences,
(
	SELECT ST_AsMVT(mvtgeompics.*, 'pictures') AS mvt
	FROM (
		SELECT
			ST_AsMVTGeom(ST_Transform(geom, 3857), ST_TileEnvelope(%(z)s, %(x)s, %(y)s)) AS geom,
			id, ts, heading
		FROM pictures
		WHERE
			status = 'ready'
			AND geom && ST_Transform(ST_TileEnvelope(%(z)s, %(x)s, %(y)s), 4326)
	) mvtgeompics
) mvtpictures
"""

	elif z >= 7:
		query = """
SELECT ST_AsMVT(mvtsequences.*, 'sequences') AS mvt
FROM (
	SELECT
		ST_AsMVTGeom(ST_Transform(geom, 3857), ST_TileEnvelope(%(z)s, %(x)s, %(y)s)) AS geom,
		id
	FROM sequences
	WHERE
		status = 'ready'
		AND geom && ST_Transform(ST_TileEnvelope(%(z)s, %(x)s, %(y)s), 4326)
) mvtsequences
"""
	else:
		query = """
SELECT ST_AsMVT(mvtsequences.*, 'sequences') AS mvt
FROM (
	SELECT
		ST_AsMVTGeom(
			ST_Transform(geom, 3857),
			ST_TileEnvelope(%(z)s, %(x)s, %(y)s)
		) AS geom,
		id
	FROM (
		SELECT ST_Simplify(geom, 0.01) AS geom, id
		FROM sequences
		WHERE
			status = 'ready'
			AND geom && ST_Transform(ST_TileEnvelope(%(z)s, %(x)s, %(y)s), 4326)
	) s
	WHERE geom IS NOT NULL
) mvtsequences
"""

	return query
