import io
import fs
from copy import deepcopy
import typing
import random
import re
from datetime import datetime, timedelta
from PIL import Image, ExifTags
from PIL.Image import Exif
from tqdm import trange
from src import runner_pictures
import pyexiv2

france_bbox = [43.67,-0.73,49.19,5.88]
timestamp_range = (
	1577880000, # from 2020
	1641038400, # to 2022
)


def fill_with_mock_data(base_image_path: str, nb_sequences: int, nb_pictures_per_sequence: int, max_size: str):
	"""Generate mock data for geovisio
	Generate a number of sequences mocking a real path.

	the starting point of each sequence is random, and each folowing picture is duplicated with updated position/datetime exif metadata

	Args:
		base_image_path (str): local path to a base image that will be duplicated
		nb_sequences (int): number of sequence to generate
		nb_pictures_per_sequence (int): number max of photo per sequence
		max_size (int): Maxium size, in bytes, of pictures to generate
	"""
	print("""
Start generating sequences...
  - %s sequences of %s images
  - Using max %s of disk space
""" % (nb_sequences, nb_pictures_per_sequence, max_size))

	endReason = _generate_pics(base_image_path, nb_sequences, nb_pictures_per_sequence, max_size)

	if endReason != "Done":
		print(endReason)

	print("Running import of sequences into database...")
	runner_pictures.run()


def _generate_pics(base_image_path: str, nb_sequences: int, nb_pictures_per_sequence: int, max_size: str):
	"""Creates mock sequences and images in filesystem

	Parameters
	----------
	base_image_path : str
		The path to image to duplicate (in your local filesystem)
	nb_sequences : int
		Amount of maximum wanted sequences (can be limited by given max_size)
	nb_pictures_per_sequences : int
		Amount of maximum wanted pictures in a single sequence (can be limited by given max_size)
	max_size : str
		Maximum size of images on disk that this function should create, with unit (for example 200KB, 15MB, 1GB...)

	Returns
	-------
	str
		Reason of this function ending ("Done" or some message)
	"""

	base_image = Image.open(base_image_path)

	current_size = 0

	base_dir = _get_base_dir()
	max_size_in_bytes = _parse_max_size(max_size)

	exif = base_image.getexif()

	with pyexiv2.Image(base_image_path) as img_data:
		base_xmp = img_data.read_raw_xmp()

	for sequence in trange(nb_sequences, unit="sequence"):
		sequence_name = f"sequence_{sequence}"
		sequence_dir = base_dir.makedirs(sequence_name, recreate=True)

		lat = random.uniform(france_bbox[0], france_bbox[2])
		lon = random.uniform(france_bbox[1], france_bbox[3])
		date = datetime.fromtimestamp(random.uniform(timestamp_range[0], timestamp_range[1]))
		deltaLat = random.uniform(-0.001, 0.001)
		deltaLon = random.uniform(-0.001, 0.001)

		for picture_nb in range(nb_pictures_per_sequence):
			picture_file_name = f"{date.strftime('%Y-%d-%m_%H-%M-%S')}.jpg"

			lon += deltaLon
			lat += deltaLat

			updated_exif = deepcopy(exif)

			gps_info = updated_exif.get_ifd(ExifTags.Base.GPSInfo)
			gps_info.update(_updated_gpsinfo(lat, lon))

			# update the date tags in the picture
			formated_date = date.strftime("%Y:%m:%d %H:%M:%S")
			updated_exif.get_ifd(ExifTags.Base.ExifOffset)[ExifTags.Base.DateTimeDigitized] = formated_date
			updated_exif.get_ifd(ExifTags.Base.ExifOffset)[ExifTags.Base.DateTimeOriginal] = formated_date

			bytes = io.BytesIO()

			base_image.save(bytes, format='jpeg', exif=updated_exif)
			current_size += bytes.getbuffer().nbytes # PIL does some compresion, so we need to check the actual generated picture size

			# we also save the XMP metadata
			img_data = pyexiv2.ImageData(bytes.getvalue())
			img_data.modify_raw_xmp(base_xmp)

			sequence_dir.writebytes(picture_file_name, img_data.get_bytes())
			date += timedelta(minutes=1) # the next photo will be 1mn later

			if current_size >= max_size_in_bytes:
				return f"Reached max disk usage (generated {_human_readable_size(current_size)} of images)"

	return "Done"


def _updated_gpsinfo(lat: float, lon: float):
	"""Transforms coordinates into EXIF GPS tags"""
	lat_ref = "S" if lat < 0 else "N"
	lon_ref = "W" if lon < 0 else "E"
	lat_deg = _deg_min_sec(lat)
	lon_deg = _deg_min_sec(lon)

	return {
		ExifTags.GPS.GPSLatitudeRef: lat_ref,
		ExifTags.GPS.GPSLatitude: lat_deg,
		ExifTags.GPS.GPSLongitudeRef: lon_ref,
		ExifTags.GPS.GPSLongitude: lon_deg,
	}


def _deg_min_sec(val: float) -> typing.Tuple[int, int, float]:
	"""Converts lat/lon decimal coordinates into degrees/minutes/seconds format"""
	val = abs(val)
	degree = int(val)
	minute = int((val - degree) * 60)
	seconde = round(((val - int(val)) - (float(minute) / 60)) * 3600, 4) # arbitrary precision to < 1cm

	return (degree, minute, seconde)


def _get_base_dir():
	"""Opens and returns root folder of Geovisio filesystem"""
	from flask import current_app
	return fs.open_fs(current_app.config['FS_URL'])


def _parse_max_size(max_size: str) -> int:
	"""Converts a human-readable size string into bytes integer value

	>>> _parse_max_size("1MB")
	1048576
	>>> _parse_max_size("1 KB")
	1024
	>>> _parse_max_size("1 B")
	1
	>>> _parse_max_size("1")
	Traceback (most recent call last):
	...
	ValueError: invalid max_size, should have a value and a unit: B, KB, MB or GB eg. 50MB
  	"""
	match = re.match("([0-9]+) ?([KMG]?B)", max_size)

	if not match:
		raise ValueError("invalid max_size, should have a value and a unit: B, KB, MB or GB eg. 50MB")

	value = int(match[1])
	unit = match[2]
	if unit == "B":
		return value
	if unit == "KB":
		return value * 1024
	if unit == "MB":
		return value * 1024 * 1024
	if unit == "GB":
		return value * 1024 * 1024 * 1024
	raise ValueError("invalid max_size unit, should be: B, KB, MB or GB")


def _human_readable_size(size: int) -> str:
	"""Converts a bytes integer value into human-readable string

	>>> _human_readable_size(1)
	'1.0B'
	>>> _human_readable_size(1024)
	'1.0KB'
	>>> _human_readable_size(1536)
	'1.5KB'
	>>> _human_readable_size(1073741824)
	'1.0GB'
	"""
	v = float(size)
	for unit in ["B", "KB", "MB", "GB"]:
		if v < 1024:
			return f"{v:3.1f}{unit}"
		v /= 1024
	return f"{v:.1f}Yi"
