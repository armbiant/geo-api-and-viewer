# Only if pictures blurring is enabled
tensorflow==2.*
opencv-python==4.*
rectangle_packer==2.0.1
# pytorch is also necessary for blur strategies COMPROMISE and QUALITATIVE,
# to install pytorch go to https://pytorch.org/
