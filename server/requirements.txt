Flask==2.*
psycopg==3.*
flasgger==0.9.*
Pillow==9.*
Flask-Cors==3.0.*
fs==2.*
fs-s3fs @ git+https://gitlab.com/geovisio/s3fs.git@4f24ece5a4c8db0f818908a797798a7264786303 # install s3-s3fs from a fork to be able to add fixes
flask-compress==1.*
tqdm==4.*
xmltodict==0.13.*
requests==2.*
yoyo-migrations==8.*
psycopg-binary==3.*
python-dotenv==0.21.*
authlib==1.2.*
Flask-Executor==1.0.*
